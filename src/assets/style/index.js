const Colors = {
  primary: '#5F8DD3',
  secondary: '#2AD4FF',
  yellow: '#ffff00',
  white: 'white',
  black: '#1e1e1e',
  grey: '#808080',
};

export {Colors};
