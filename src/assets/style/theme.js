const light = {
  primary: '#5F8DD3',
  secondary: '#2AD4FF',
  yellow: '#ffff00',
  white: 'white',
  black: '#1e1e1e',
  grey: '#808080',
};
const dark = {
  primary: '#5F8DD3',
  secondary: '#2AD4FF',
  yellow: '#ffff00',
  white: 'white',
  black: '#1e1e1e',
  grey: '#808080',
};
const themes = {
  default: {...defaultColors},
  dark: {...darkColors},
  blue: {...defaultColors, primary: '#4299E1'},
  'blue-dark': {...darkColors, primary: '#63B3ED'},
};

export const getThemeColor = (color, theme = 'default') => {
  const themeColor = themes[theme][color];
  const fallbackColor = themes.dark[theme][color];
  return themeColor || fallbackColor;
};
