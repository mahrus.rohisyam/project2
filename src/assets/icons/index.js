import Globe from './png/globe.png';
import News from './png/news.png';
import Settings from './png/settings.png';

export {Globe, News, Settings};
