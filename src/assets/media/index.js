import Logo from './png/Logo.png';
import LogoMini from './png/LogoMini.png';
import CloudIllustration from './png/cloud.png';

export {Logo, CloudIllustration, LogoMini};
