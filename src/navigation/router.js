import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Login,
  Signup,
  News,
  Settings,
  Splash,
  Forgot,
  Discover,
  Notification,
  Article,
} from '../screens';
import BottomNavigator from './BottomTabNavigator/BottomNavigator';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const hide = {headerShown: false};

function MainNav() {
  return (
    <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
      <Tab.Screen name="News" component={News} options={{headerShown: false}} />
      <Tab.Screen
        name="Discover"
        component={Discover}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Settings"
        component={Settings}
        options={{headerShown: false}}
      />
    </Tab.Navigator>
  );
}

class index extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Splash">
          <Stack.Screen name="Splash" component={Splash} options={hide} />
          <Stack.Screen name="MainNav" component={MainNav} options={hide} />
          <Stack.Screen name="Login" component={Login} options={hide} />
          <Stack.Screen name="SignUp" component={Signup} options={hide} />
          <Stack.Screen name="Settings" component={Settings} options={hide} />
          <Stack.Screen name="News" component={News} options={hide} />
          <Stack.Screen name="Forgot" component={Forgot} options={hide} />
          <Stack.Screen
            name="Notification"
            component={Notification}
            options={hide}
          />
          <Stack.Screen name="Article" component={Article} options={hide} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
export default index;
