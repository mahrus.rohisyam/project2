import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import Ionicons from 'react-native-vector-icons';
import {Globe, News, Settings} from '../../assets/icons';
// import Settings from '../../assets/icons/settings.png';
// import News from '../../assets/icons/news.png';
// import Globe from '../../assets/icons/globe.png';
// import {Settings, Globe, News} from '../../assets';
const TabItem = ({isFocused, onPress, label, onLongPress, index}) => {
  const Icon = () => {
    if (label === 'News') {
      return <Image source={News} />;
    }
    if (label === 'Discover') {
      return <Image source={Globe} />;
    }
    if (label === 'Settings') {
      return <Image source={Settings} />;
    }
    return <Image source={News} />;
  };
  return (
    <View style={styles.mainContainer}>
      <TouchableOpacity
        key={index}
        accessibilityRole="button"
        accessibilityState={isFocused ? {selected: true} : {}}
        onPress={onPress}
        onLongPress={onLongPress}
        style={styles.button}>
        <Icon />
        <Text style={{color: isFocused ? '#673ab7' : 'black'}}>{label}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    paddingVertical: 10,
    flex: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
  },
});
