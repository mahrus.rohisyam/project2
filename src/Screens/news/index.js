import {CModal, CText, Header, PostCard} from '../../component/';
import React, {Component} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import {API_KEY, maxRes} from '../../uttils/services/config';
export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: {},
      maxRes: maxRes,
      visible: false,
      negara: [{name: 'ID'}, {name: 'EN'}, {name: 'IND'}],
      country: 'ID',
    };
  }
  componentDidMount() {
    const {articles, maxRes, country} = this.state;
    fetch(
      `https://newsapi.org/v2/top-headlines?country=${country}&apiKey=${API_KEY}&pageSize=${maxRes}`,
    )
      .then(response => response.json())
      .then(json => this.setState({articles: json}));
  }
  componentDidUpdate(){

  }
  _filter = () => {
    const {visible} = this.state;
    this.setState({visible: !visible});
  };
  _article = () => {
    const {navigation} = this.props;
    navigation.navigate('Article');
  };
  _changeCountry=()=>{
    const {negara} = this.state
    negara.filter((val)=>{
      
    })
  }
  render() {
    const {articles, visible, negara, country} = this.state;
    const {navigation} = this.props;
    return (
      <View>
        <CModal visible={visible} onRequestClose={this._filter}>
          <View
            style={{
              flexDirection: 'row-reverse',
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingBottom: 10,
              paddingRight: 10,
            }}>
            <MCI name="close-octagon" size={30} onPress={this._filter} />
            <CText>Filter</CText>
          </View>
          <View style={styles.rowModal}>
            <CText>Country</CText>
            <View style={styles.rowCat}>
              {negara.map((val, i) => {
                return (
                  <TouchableOpacity
                    key={i}
                    onPress={() => {
                      this.setState({country: val.name});
                    }}>
                    <CText style={styles.textButton}>{val.name}</CText>
                  </TouchableOpacity>
                );
              })}
            </View>
          </View>
        </CModal>
        <Header
          filter={this._filter}
          notif={() => {
            navigation.navigate('Notification');
          }}
        />
        <ScrollView>
          {articles.articles ? (
            articles.articles.map((val, i) => {
              return (
                <PostCard
                  onPress={this._article}
                  key={i}
                  imageSource={val.urlToImage}
                  title={val.title}
                  desc={val.description}
                  date={val.publishedAt}
                  source={val.source.name}
                />
              );
            })
          ) : (
            <View style={styles.loading}>
              <ActivityIndicator size="large" color="#5F8DD3" />
              <CText>Please be patience</CText>
              <CText>Loading Headlines...</CText>
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

export default index;
const styles = StyleSheet.create({
  rowModal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 50,
  },
  rowCat: {
    flexDirection: 'row',
    paddingLeft: 20,
  },
  textButton: {
    backgroundColor: 'white',
    borderRadius: 5,
    paddingHorizontal: 10,
    elevation: 5,
  },
  loading: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
  },
});
