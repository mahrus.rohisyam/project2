import React, {Component} from 'react';
import {StyleSheet, View, ScrollView, TouchableOpacity} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {responsiveHeight, responsiveWidth} from '../../assets/uttils';
import {Colors} from '../../assets/style';
import {CText, Divider} from '../../component';
import WebView from 'react-native-webview';
export default class article extends Component {
  constructor(props) {
    super(props);
    this.state = {
      article: [{title: 'Title Goes Here', content: 'Content Goes Here'}],
    };
  }
  render() {
    const {article} = this.state;
    const {navigation} = this.props;
    return (
      <View>
        <View style={styles.mainContainer}>
          <View style={styles.misc}>
            <TouchableOpacity
              style={styles.icon}
              onPress={() => {
                navigation.goBack();
              }}>
              <MaterialIcons name="arrow-back" color="#fff" size={30} />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView>
          <CText>article goes here</CText>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    width: responsiveWidth(410),
    height: responsiveHeight(100),
    flexDirection: 'row',
    elevation: 10,
  },
  misc: {
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 15,
  },
  icon: {
    width: responsiveWidth(60),
    height: responsiveHeight(60),
    backgroundColor: '#1e1e1e',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
