import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {responsiveHeight, responsiveWidth} from '../../assets/uttils';
import {Colors} from '../../assets/style';
import {CText, Divider} from '../../component';
export class index extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <View>
        <View style={styles.mainContainer}>
          <View style={styles.misc}>
            <TouchableOpacity
              style={styles.icon}
              onPress={() => {
                navigation.goBack();
              }}>
              <MaterialIcons name="arrow-back" color="#fff" size={30} />
            </TouchableOpacity>
            <CText style={styles.title}>Notifications</CText>
          </View>
        </View>
        <ScrollView>
          <Divider height={30} width={100} />
          <CText>There isNothing here</CText>
        </ScrollView>
      </View>
    );
  }
}
export default index;
const styles = StyleSheet.create({
  mainContainer: {
    width: responsiveWidth(410),
    height: responsiveHeight(100),
    flexDirection: 'row',
    elevation: 10,
  },
  misc: {
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 15,
  },
  icon: {
    width: responsiveWidth(60),
    height: responsiveHeight(60),
    backgroundColor: '#1e1e1e',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 22,
    marginLeft: 15,
    color: Colors.black,
  },
});
