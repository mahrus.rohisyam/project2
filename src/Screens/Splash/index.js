import React, {Component} from 'react';
import {Image, Text, View, StyleSheet} from 'react-native';
import {Logo} from '../../assets/media';
export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false,
    };
  }
  componentDidMount() {
    const {isLoggedIn} = this.state;
    {
      isLoggedIn
        ? setTimeout(() => {
            this.props.navigation.replace('News');
          }, 3000)
        : setTimeout(() => {
            this.props.navigation.replace('Login');
          }, 3000);
    }
  }
  render() {
    return (
      <View style={styles.mainContainer}>
        <Image style={styles.logo} source={Logo} />
        <Text style={styles.title}>Portal Berita</Text>
      </View>
    );
  }
}

export default index;
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1e1e1e',
  },
  title: {
    color: 'white',
    fontSize: 24,
    fontFamily: 'Pacifico-Regular',
  },
});
