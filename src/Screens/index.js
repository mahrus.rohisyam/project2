import Login from './login';
import Signup from './signup';
import News from './news';
import Settings from './settings';
import Splash from './splash';
import Forgot from './login/forgot';
import Discover from './discover';
import Notification from './notification';
import Article from './news/article';
export {
  Login,
  Signup,
  News,
  Settings,
  Splash,
  Forgot,
  Discover,
  Notification,
  Article,
};
