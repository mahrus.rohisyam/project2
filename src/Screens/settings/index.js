import React, {Component} from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {CButton, CText} from '../../component';
import MI from 'react-native-vector-icons/MaterialIcons';
import {responsiveHeight, responsiveWidth} from '../../assets/uttils';
import {Colors} from '../../assets/style';
import {height} from 'styled-system';
export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {
        username: 'Username Goes Here',
        fullname: 'fullname Goes here',
        email: 'email GOes here',
      },
    };
  }
  render() {
    const {userData} = this.state;
    return (
      <View>
        <ScrollView>
          <View style={styles.mainContainer}>
            <View style={styles.profilePhoto}>
              {/* <Image source={{uri:''}} /> */}
              <MI name="person" size={75} />
            </View>
            <CText>@{userData.username}</CText>
          </View>
          <View style={styles.secondaryContainer}>
            <CText style={styles.data}>Fullname</CText>
            <CText style={styles.subData}>{userData.fullname}</CText>
            <CText style={styles.data}>Email</CText>
            <CText style={styles.subData}>{userData.fullname}</CText>
            <CButton style={styles.btn}>Edit</CButton>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default index;
const styles = StyleSheet.create({
  mainContainer: {
    height: responsiveHeight(350),
    backgroundColor: Colors.primary,
    borderBottomStartRadius: 50,
    borderBottomEndRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 15,
  },
  secondaryContainer: {
    height: responsiveHeight(350),
    backgroundColor: Colors.white,
    borderRadius: 20,
    elevation: 10,
    marginVertical: 10,
    padding: 20,
  },
  profilePhoto: {
    backgroundColor: Colors.yellow,
    width: responsiveWidth(125),
    height: responsiveHeight(150),
    borderRadius: 125,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  subData: {
    paddingTop: 5,
    paddingLeft: 20,
  },
  data: {
    paddingVertical: 3,
  },
  btn: {
    width: responsiveWidth(200),
    height: responsiveHeight(65),
    alignSelf: 'center',
    marginTop: 50,
  },
});
