import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import {responsiveHeight, responsiveWidth} from '../../assets/uttils';
import {CButton, CText, CTextInput, Header, PostCard} from '../../component';
import {country, API_KEY} from '../../uttils/services/config';
export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: '',
      articles: {},
    };
  }
  _submit = () => {
    const {input} = this.state;
    fetch(
      `https://newsapi.org/v2/everything?q=${input.toLowerCase()}&sortBy=popularity&apiKey=${API_KEY}&pageSize=5`,
    )
      .then(response => response.json())
      .then(json => this.setState({articles: json}));
  };
  render() {
    const {articles} = this.state;
    return (
      <View style={styles.mainContainer}>
        <Header
          onChangeText={typing => this.setState({input: typing})}
          searchBar={true}
          search={() => {
            this._submit();
          }}
        />
        {articles.articles ? (
          <ScrollView>
            {articles.articles.map((val, i) => {
              return (
                <View style={styles.thirdContainer} key={i}>
                  <PostCard
                    imageSource={val.urlToImage}
                    title={val.title}
                    desc={val.description}
                    date={val.publishedAt}
                    source={val.source.name}
                  />
                </View>
              );
            })}
          </ScrollView>
        ) : (
          <View style={styles.secondaryContainer}>
            <CText>Type something in search bar to</CText>
            <CText>Discover more News</CText>
          </View>
        )}
      </View>
    );
  }
}

export default index;
const styles = StyleSheet.create({
  mainContainer: {
    justifyContent: 'center',
  },
  secondaryContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50,
  },
  thirdContainer: {
    marginBottom: 15,
  },
});
