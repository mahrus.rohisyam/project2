import React, {Component} from 'react';
import {Image, Text, View, StyleSheet} from 'react-native';
import {CButton, CText, CTextInput, BubbleButton} from '../../component/';
export class index extends Component {
  constructor(props) {
    super(props);
  }
  _submit = () => {
    setTimeout(() => {
      this.props.navigation.replace('MainNav');
    });
  };

  _register = () => {
    setTimeout(() => {
      this.props.navigation.navigate('SignUp');
    });
  };
  _forgot = () => {
    setTimeout(() => {
      this.props.navigation.replace('Forgot');
    });
  };

  _close = () => {
    this.props.navigation.goBack();
  };
  render() {
    return (
      <View style={styles.mainContainer}>
        <BubbleButton iconName="close" onPress={this._close} />

        <CText style={styles.text}>
          You don’t think you should login first and behave like human not
          robot.
        </CText>
        <CTextInput placeholder="Email/Username" style={styles.input} />
        <CTextInput placeholder="Password" style={styles.input} />

        <View style={styles.button}>
          <CButton
            onPress={() => {
              this._submit();
            }}>
            Login
          </CButton>
        </View>

        <CText style={styles.text}>
          Forgot Password?{' '}
          <CText
            style={styles.link}
            onPress={() => {
              this._forgot();
            }}>
            Click Here !
          </CText>
        </CText>

        <CText style={styles.text}>
          New User?{' '}
          <CText
            style={styles.link}
            onPress={() => {
              this._register();
            }}>
            Register
          </CText>
        </CText>
      </View>
    );
  }
}

export default index;
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#1e1e1e',
  },
  title: {
    color: 'white',
    fontSize: 24,
  },
  input: {
    marginVertical: 10,
  },
  button: {
    marginTop: 10,
    alignSelf: 'center',
  },
  text: {
    textAlign: 'center',
  },
  link: {
    color: '#5F8DD3',
    textDecorationLine: 'underline',
  },
});
