import React, {Component} from 'react';
import {Image, Text, View, StyleSheet} from 'react-native';
import {CButton, CText, CTextInput, BubbleButton} from '../../component/';

export class index extends Component {
  constructor(props) {
    super(props);
  }

  _submit = () => {
    setTimeout(() => {
      this.props.navigation.replace('Login');
    });
  };

  _login = () => {
    setTimeout(() => {
      this.props.navigation.replace('Login');
    });
  };

  _back = () => {
    this.props.navigation.goBack();
  };
  render() {
    return (
      <View style={styles.mainContainer}>
        <BubbleButton iconName="arrow-back" onPress={this._back} />

        <View>
          <CText>
            You have chance to create new account if you really want to.
          </CText>

          <CTextInput placeholder="Full name" style={styles.input} />

          <CTextInput placeholder="Email" style={styles.input} />

          <CTextInput placeholder="Password" style={styles.input} />

          <View style={styles.button}>
            <CButton
              onPress={() => {
                this._submit();
              }}>
              SignUp
            </CButton>
          </View>

          <CText style={styles.text}>
            Old User?{' '}
            <CText
              style={styles.link}
              onPress={() => {
                this._login();
              }}>
              Login Instead
            </CText>
          </CText>
        </View>
      </View>
    );
  }
}

export default index;
const styles = StyleSheet.create({
  mainContainer: {
    justifyContent: 'center',
    backgroundColor: '#1e1e1e',
    flex: 1,
  },
  title: {
    color: 'white',
    fontSize: 24,
  },
  input: {
    marginVertical: 10,
  },
  button: {
    marginTop: 10,
    alignSelf: 'center',
  },
  text: {
    textAlign: 'center',
  },
  link: {
    color: '#5F8DD3',
    textDecorationLine: 'underline',
  },
});
