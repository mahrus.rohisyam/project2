import React, {Component} from 'react';
import {StyleSheet, View, TouchableOpacity, Image} from 'react-native';
import {Colors} from '../assets/style';
import {responsiveHeight, responsiveWidth} from '../assets/uttils';
import {CText} from '../component';
export class postCard extends Component {
  render() {
    const style = this.props;
    return (
      <View style={styles.card}>
        <TouchableOpacity {...this.props}>
          <Image
            source={{uri: `${this.props.imageSource}`}}
            resizeMode="cover"
            style={styles.thumbnail}
          />
          <View style={styles.textContainer}>
            <CText style={styles.title}>{this.props.title}</CText>
            <CText style={styles.text}>{this.props.desc}</CText>
            <View style={styles.miscRow}>
              <CText style={styles.miscWrapper}>Source:</CText>
              <CText style={styles.misc}> {this.props.source}</CText>
            </View>
            <View style={styles.miscRow}>
              <CText style={styles.miscWrapper}>Published:</CText>
              <CText style={styles.misc}>{this.props.date}</CText>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default postCard;
const styles = StyleSheet.create({
  card: {
    width: responsiveWidth(390),
    minHeight: responsiveHeight(300),
    maxHeight: responsiveHeight(800),
    borderBottomStartRadius: 20,
    borderBottomEndRadius: 20,
    margin: 10,
    justifyContent: 'center',
    elevation: 15,
    backgroundColor: 'white',
  },
  thumbnail: {
    height: responsiveHeight(300),
    width: responsiveWidth(380),
  },
  title: {
    color: '#1e1e1e',
    fontSize: 16,
  },
  text: {
    fontSize: 14,
    color: 'grey',
  },
  misc: {
    fontSize: 14,
    color: '#1e1e1e',
    paddingHorizontal: 5,
  },
  miscWrapper: {
    fontSize: 14,
    color: '#1e1e1e',
    backgroundColor: Colors.yellow,
    borderRadius: 20,
    paddingHorizontal: 10,
  },
  miscRow: {
    flexDirection: 'row',
    paddingVertical: 3,
  },
  textContainer: {
    margin: 10,
  },
});
