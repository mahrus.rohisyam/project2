import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
export class CTextInput extends Component {
  render() {
    const style = this.props;
    return (
      <View>
        <TouchableOpacity style={{...styles.btn, ...style}} {...this.props}>
          <MaterialIcons
            name={this.props.iconName}
            onPress={this.props.onPress}
            size={30}
            color="#5F8DD3"
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default CTextInput;
const styles = StyleSheet.create({
  btn: {
    backgroundColor: 'white',
    borderRadius: 50,
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    color: 'black',
    position: 'absolute',
    top: -125,
    left: 20,
  },
});
