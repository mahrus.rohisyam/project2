import React, {Component} from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {CText, CTextInput} from '.';
import {LogoMini} from '../assets/media';
import {Colors} from '../assets/style';
import {responsiveHeight, responsiveWidth} from '../assets/uttils';
export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      general: this.props.searchBar,
      search: false,
    };
  }
  componentDidMount() {
    const {general, search} = this.state;
    if (search == true) {
      this.setState({
        general: !general,
      });
    }
  }
  render() {
    const {general} = this.state;
    return (
      <View>
        {general ? (
          <View style={styles.mainContainer}>
            <View style={styles.block}>
              <CTextInput
                {...this.props}
                style={styles.input}
                placeholder="Type something"
              />
            </View>
            <View style={styles.misc}>
              <TouchableOpacity style={styles.icon} onPress={this.props.search}>
                <MaterialIcons name="search" color="#fff" size={30} />
              </TouchableOpacity>
              <TouchableOpacity style={styles.icon} onPress={this.props.filter}>
                <MaterialIcons name="filter-alt" color="#fff" size={30} />
              </TouchableOpacity>
            </View>
          </View>
        ) : (
          <View style={styles.mainContainer}>
            <View style={styles.block}>
              <Image style={styles.logo} source={LogoMini} />
            </View>
            <View style={styles.block}>
              <Text style={styles.title}></Text>
            </View>
            <View style={styles.generalMisc}>
              <TouchableOpacity style={styles.icon} onPress={this.props.filter}>
                <MaterialIcons name="filter-alt" color="#fff" size={30} />
              </TouchableOpacity>
              <TouchableOpacity style={styles.icon} onPress={this.props.notif}>
                <MaterialIcons name="notifications" color="#fff" size={30} />
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default index;
const styles = StyleSheet.create({
  mainContainer: {
    width: responsiveWidth(410),
    height: responsiveHeight(100),
    alignItems: 'center',
    backgroundColor: '#fff',
    flexDirection: 'row',
    elevation: 10,
  },
  title: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
  },
  logo: {
    width: responsiveWidth(50),
    height: responsiveHeight(61),
    margin: 15,
  },
  misc: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 0.5,
  },
  generalMisc: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  icon: {
    width: responsiveWidth(60),
    height: responsiveHeight(60),
    backgroundColor: '#1e1e1e',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  block: {
    flex: 1,
  },
  input: {
    elevation: 10,
    position: 'relative',
    width: responsiveWidth(250),
    height: responsiveHeight(65),
    backgroundColor: Colors.primary,
  },
});
