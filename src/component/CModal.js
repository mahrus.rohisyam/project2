import React, {Component} from 'react';
import {Text, StyleSheet, View, Modal, Pressable} from 'react-native';
import {CText} from '.';

export class CModal extends Component {
  render() {
    const {style} = this.props;
    return (
      <View>
        <Modal
          {...this.props}
          animationType="slide"
          transparent={true}
          presentationStyle="overFullScreen"
          hardwareAccelerated={true}>
          <View style={styles.centeredView}>
            <View style={{...styles.modalView, ...style}} {...this.props}>
              {this.props.children}
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
export default CModal;
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});
