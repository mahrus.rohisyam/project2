import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {responsiveHeight, responsiveWidth} from '../assets/uttils';

export class Divider extends Component {
  render() {
    const {height, width} = this.props;
    return (
      <View
        style={{
          height: responsiveHeight(height),
          width: responsiveWidth(width),
        }}
      />
    );
  }
}

export default Divider;
