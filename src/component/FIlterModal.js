import React, {Component} from 'react';
import {Text, StyleSheet, View, Button} from 'react-native';
import {CModal, CText, CTextInput, Header, PostCard} from '../component/';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';

export default class FIlterModal extends Component {
  render() {
    return (
      <CModal {...this.props}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingBottom: 10,
            paddingLeft: 10,
          }}>
          <MCI name="close-octagon" size={20} />
          <CText>Filter News</CText>
        </View>
        <View style={styles.rowModal}>
          <CText>Language</CText>
          <Button title="Test" />
        </View>
        <View style={styles.rowModal}>
          <CText>Category</CText>
          <Button title="Test" />
        </View>
        <View style={styles.rowModal}>
          <CText>Language</CText>
          <Button title="Test" />
        </View>
        <View style={styles.rowModal}>
          <CText>Sort By</CText>
          <Button title="Test" />
        </View>
      </CModal>
    );
  }
}

const styles = StyleSheet.create({
  rowModal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 100,
  },
});
