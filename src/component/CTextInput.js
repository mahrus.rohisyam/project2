import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {responsiveHeight, responsiveWidth} from '../assets/uttils';
export class CTextInput extends Component {
  render() {
    const {style} = this.props;
    return (
      <View style={styles.mainView}>
        <TextInput
          placeholderTextColor="black"
          style={styles.input}
          {...this.props}
          style={{...styles.input, ...style}}
        />
      </View>
    );
  }
}

export default CTextInput;
const styles = StyleSheet.create({
  input: {
    backgroundColor: 'white',
    borderRadius: 20,
    width: responsiveWidth(280),
    height: responsiveHeight(70),
    justifyContent: 'center',
    flexDirection: 'row',
    paddingLeft: 15,
    color: 'black',
  },
  iconBack: {
    position: 'absolute',
    top: 25,
    right: 15,
  },
  mainView: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
